#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C)
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


cache = [0 for x in range(1000000)]


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    global cache

    assert i > 0
    assert j > 0

    if j < i:
        temp = i
        i = j
        j = temp

    cache[0] = 0
    cache[1] = 1

    max = 0
    count = 0
    for t in range(i, j+1):
        count = 0
        if cache[t] != 0:
            count = cache[t]

        else:
            t2 = t
            while t > 1:
                if t % 2 == 0:
                    t //= 2
                    count += 1
                else:
                    t = t + (t >> 1) + 1
                    count += 2

                if t < 1000000 and cache[t] != 0:
                    count += cache[t]
                    break
            t = t2
            cache[t] = count

        if count > max:
            max = count

    assert max > 0
    return max


# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        if s == "\n":
            continue
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
